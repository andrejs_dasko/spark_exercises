val str = "aaabbbccccdd"
val list = str.groupBy(identity).toList.sortBy(_._1).map(_._2)
val grouped = str.groupBy(identity)
grouped.map(pair => (pair._1, pair._2.length))