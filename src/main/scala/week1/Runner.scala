package week1

import java.io.File

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by andrejs.dasko on 25-Jul-17.
  */
object Runner {

  val conf: SparkConf = new SparkConf().setMaster("local").setAppName("NewYearHonours")
  val sc: SparkContext = new SparkContext(conf)
  val newYearHonoursFilePath = "data/1918NewYearHonours.txt"
  val listOfAustralianTreatiesFilePath = "data/ListOfAustralianTreaties.txt"

  def main(args: Array[String]): Unit = {
    sc.setLogLevel("ERROR")

    val honoursRdd: RDD[String] = sc.textFile(file(newYearHonoursFilePath)).cache()
    val treatiesRdd: RDD[String] = sc.textFile(file(listOfAustralianTreatiesFilePath)).cache()

//    println("Task 1 result: " + task1(treatiesRdd))
//    println("Task 2 result: " + task2(honoursRdd, treatiesRdd))
//    println("Task 3 result: " + task3(treatiesRdd))
//    println("Task 4 result: " + task4(treatiesRdd))
//    println("Task 5 result: " + task5(treatiesRdd))
//    println("Task 6 result: " + task6(treatiesRdd).foreach(println))
    task7(treatiesRdd)

    sc.stop()
  }

  /*
    How many words are in ListOfAustralianTreaties.txt?
   */
  def task1(treatiesRdd: RDD[String]): Long = {
    words(treatiesRdd).count
  }

  /*
    How many words are in both .txt files?
   */
  def task2(honoursRdd: RDD[String], treatiesRdd: RDD[String]): Long = {
    val union = honoursRdd.union(treatiesRdd)
    words(union).count
  }

  /*
    How many normal words are in ListOfAustralianTreaties.txt?
   */
  def task3(treatiesRdd: RDD[String]): Long = {
    normalWords(words(treatiesRdd)).count
  }

  /*
 	  How many unique numbers are in ListOfAustralianTreaties.txt?
   */
  def task4(treatiesRdd: RDD[String]): Long = {
    uniqueNumbers(words(treatiesRdd)).count
  }

  /*
 	  Calculate average of all numbers in ListOfAustralianTreaties.txt
   */
  def task5(treatiesRdd: RDD[String]): Double = {
    val numbersRdd = numbers(treatiesRdd).cache()
    val count = numbersRdd.count()
    val sum = numbersRdd.map(number => number.toInt).sum()
    sum / count
  }

  /*
   What are 10 most frequent symbols in ListOfAustralianTreaties.txt?
	 How many occurrences there are for each symbol?
   */
  def task6(linesRdd: RDD[String]): Array[(Char, Int)] = {
    val occurrencePairs = characters(linesRdd).groupBy(identity).map(pair => (pair._1, pair._2.size))
    // rdd.sortByKey is the same as rdd.sortBy(_._1) and the same as rdd.sortBy(pair => pair._1)
    // rdd.sortBy(_._2) will sort by value
//    val sorted = occurrencePairs.sortByKey(ascending = false)
//    val sorted = occurrencePairs.sortBy(pair => pair._1, ascending = false)
    val sorted = occurrencePairs.sortBy(_._2, ascending = false)
    sorted.take(10)
  }

  def task7(linesRdd: RDD[String]): Unit = {
    val wordsRdd = words(linesRdd)
    def group0Filter(word: String): Boolean = otherSymbolCount(word) > 0 || (digitCount(word) > 0 && vowelCount(word) + consonantCount(word) > 0)
    def group1Filter(word: String): Boolean = digitCount(word) > 0
    def group2Filter(word: String): Boolean = vowelCount(word) > 0 && vowelCount(word) == consonantCount(word)
    def group3Filter(word: String): Boolean = vowelCount(word) > consonantCount(word)
    def group4Filter(word: String): Boolean = !group0Filter(word) && !group1Filter(word) && !group2Filter(word) && !group3Filter(word)
    val group0 = wordsRdd.filter(group0Filter)
    val group1 = wordsRdd.filter(group1Filter)
    val group2 = wordsRdd.filter(group2Filter)
    val group3 = wordsRdd.filter(group3Filter)
    val group4 = wordsRdd.filter(group4Filter)

    def printGroup(group: RDD[String], groupNum: Int, sampleSize: Int = 10): Unit = {
      println("--------------------------")
      println(s"group$groupNum size: " + group.count)
      println(s"group$groupNum sample of $sampleSize")
      group.take(sampleSize).foreach(println)
    }

    printGroup(group0, 0)
    printGroup(group1, 1)
    printGroup(group2, 2)
    printGroup(group3, 3)
    printGroup(group4, 4)
  }

  private def words(linesRdd: RDD[String]): RDD[String] = linesRdd.flatMap(line => line.split(" ")).filter(line => line.length > 0)

  private def normalWords(linesRdd: RDD[String]): RDD[String] = words(linesRdd).filter(word => word.charAt(0).isLetter)

  private def numbers(linesRdd: RDD[String]): RDD[String] = words(linesRdd).filter(word => word.toCharArray.forall(_.isDigit))

  private def uniqueNumbers(rdd: RDD[String]): RDD[String] = numbers(rdd).distinct()

  private def characters(rdd: RDD[String]): RDD[Char] = rdd.flatMap(line => line.toCharArray)

  private def isVowel(c: Char): Boolean = "aeiou".indexOf(c) > 0

  private def isConsonant(c: Char): Boolean = c.isLetter && !isVowel(c)

  private def vowelCount(word: String): Int = word.toCharArray.count(isVowel(_))

  private def consonantCount(word: String): Int = word.toCharArray.count(isConsonant(_))

  private def otherSymbolCount(word: String): Int = word.toCharArray.count(char => !char.isLetterOrDigit)

  private def digitCount(word: String): Int = {
//  word.toCharArray.filter(char => char.isDigit).length
    word.toCharArray.count(_.isDigit)
  }

  def file(path: String) = {
    val resource = this.getClass.getClassLoader.getResource(path)
    if (resource == null) sys.error("Could not find file at following file path: " + path)
    new File(resource.toURI).getPath
  }
}
