package week1

import org.apache.spark.{SparkConf, SparkContext}

object Joins {

  val conf: SparkConf = new SparkConf().setMaster("local").setAppName("NewYearHonours")
  val sc: SparkContext = new SparkContext(conf)

  case class AG()
  case class DemiTarif()
  case class DemiTarifVisa()

  def main(args: Array[String]): Unit = {
    sc.setLogLevel("ERROR")

    val as = List(
      (101, ("Ruetli", AG)),
      (102, ("Brelaz", DemiTarif)),
      (103, ("Gress", DemiTarifVisa)),
      (104, ("Schatten", DemiTarif)))
    val ls = List(
      (101, "Bern"),
      (101, "Thun"),
      (102, "Lausanne"),
      (102, "Geneve"),
      (102, "Nyon"),
      (103, "Zurich"),
      (103, "St-Gallen"),
      (103, "Chur"))

    val abos = sc.parallelize(as)
    val locations = sc.parallelize(ls)

    val trackedCustomers = abos.join(locations)

    val abosWithOptionalLocations = abos.leftOuterJoin(locations)

    val customersWithLocationDataAndOptinalAbos = abos.rightOuterJoin(locations)

    sc.stop
  }

}
