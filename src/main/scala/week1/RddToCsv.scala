package week1

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, DataFrameReader, Row, SparkSession}
import week1.Runner._

/**
  * Created by andrejs.dasko on 26-Oct-17.
  */
object RddToCsv {
  val conf: SparkConf = new SparkConf().setMaster("local").setAppName("CSVToRDD")
  val spark: SparkSession = SparkSession
    .builder()
    .appName("Example")
    .config(conf)
    .getOrCreate()
  val input: DataFrame = spark.read
    .format("csv")
    .option("header", "true")
    .option("inferSchema", true)
    .load("src/main/resources/data/FL_insurance_sample.csv")
  val rdd: RDD[Row] = input.rdd

  def main(args: Array[String]): Unit = {
    println("Starting...")
    println(rdd.count())
    rdd.map(row => row.get(2)).distinct.foreach(println)
    input.show(10)
  }
}
